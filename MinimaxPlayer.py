from Board import Board
from Game import Game
import time
import math


class MinimaxPlayer:
    def __init__(self):
        self.board = None
        self.player_location = None
        self.rival_location = None
        self.directions = [(1, 0), (0, 1), (-1, 0), (0, -1)]

        # Specified in miliseconds
        self.time_buffer = 100

        self.start_time = None
        self.time_for_move = None

        self.curr_iter_depth = None

    def get_player_location(self, agent):
        if agent == 1:
            for i, row in enumerate(self.board):
                for j, val in enumerate(row):
                    if val == 1:
                        return (i, j)
        else:
            for i, row in enumerate(self.board):
                for j, val in enumerate(row):
                    if val == 2:
                        return (i, j)

    def set_game_params(self, board):
        self.board = board
        self.player_location = self.get_player_location(1)
        self.rival_location = self.get_player_location(2)

    def time_left_for_move(self):
        time_passed = time.time() - self.start_time

        # Returning time in miliseconds
        return (self.time_for_move - time_passed) * 1000

    def get_legal_moves_locations(self, location):
        legal_moves_locations = []
        for direction in self.directions:
            i = location[0] + direction[0]
            j = location[1] + direction[1]
            if 0 <= i < len(self.board) and 0 <= j < len(self.board[0]) and self.board[i][j] == 0:
                legal_moves_locations.append((i, j))
        return legal_moves_locations

    def is_goal_state(self, agent_to_move):
        legal_player_moves_locations_number = len(self.get_legal_moves_locations(self.player_location))
        legal_rival_moves_locations_number = len(self.get_legal_moves_locations(self.rival_location))
        # print("Legal rival's moves number", legal_rival_moves_locations_number)
        # print("Agent to move here", agent_to_move)
        if legal_player_moves_locations_number == 0 and legal_rival_moves_locations_number == 0:
            return True
        elif legal_player_moves_locations_number == 0 and agent_to_move == 1:
            return True
        elif legal_rival_moves_locations_number == 0 and agent_to_move == 2:
            # print("You better be here")
            return True
        else:
            return False

    def goal_state_score(self, agent_to_move, depth):
        legal_player_moves_locations_number = len(self.get_legal_moves_locations(self.player_location))
        legal_rival_moves_locations_number = len(self.get_legal_moves_locations(self.rival_location))
        if legal_player_moves_locations_number == 0 and legal_rival_moves_locations_number == 0:
            return 0
        elif legal_player_moves_locations_number == 0 and agent_to_move == 1:
            return (self.curr_iter_depth - depth)/self.curr_iter_depth - 1
        else:
            return float('inf')

    def state_score(self, board, location):
        return self.heuristic(board, location)

    def succ_score(self, board, location):
        num_steps_available = 0
        for d in self.directions:
            i = location[0] + d[0]
            j = location[1] + d[1]
            if 0 <= i < len(board) and 0 <= j < len(board[0]) and board[i][j] == 0:  # then move is legal
                num_steps_available += 1

        if num_steps_available == 0:
            return -1
        else:
            return 4 - num_steps_available

    def distance(self, location):
        (player_i, player_j) = location
        (rival_i, rival_j) = self.rival_location
        return math.fabs(player_i - rival_i) + math.fabs(player_j - rival_j)

    def heuristic(self, board, location):
        return self.succ_score(board, location) + self.curr_iter_depth - self.distance(location)

    def non_goal_state_score(self):
        return self.state_score(self.board, self.player_location)

    def get_move(self, new_location):
        # print("Okay, Let's see how it goes here, new_location is: ", new_location)
        # print("Player's current location: ", self.player_location)
        x = new_location[0] - self.player_location[0]
        y = new_location[1] - self.player_location[1]
        return (x, y)

    def get_succ_locations(self, agent):
        if agent == 1:
            return self.get_legal_moves_locations(self.player_location)
        else:
            return self.get_legal_moves_locations(self.rival_location)

    def rb_minimax(self, depth, agent_to_move):
        # print("Entered rb_minimax() with depth = ", depth)
        # print("Agent to move is ", agent_to_move)
        # print(self.board)

        if self.time_left_for_move() <= self.time_buffer:
            # print("maybe I'm here?")
            return (-1, -1), -1, True
        elif self.is_goal_state(agent_to_move):
            if agent_to_move == 1:
                return self.player_location, self.goal_state_score(agent_to_move, depth), False
            else:
                # print("Must reach here!")
                return self.rival_location, self.goal_state_score(agent_to_move, depth), False
        elif depth == 0:
            if agent_to_move == 1:
                # print("Current player's location is ", self.player_location)
                return self.player_location, self.non_goal_state_score(), False
            else:
                return self.rival_location, self.non_goal_state_score(), False

        succ_locations = self.get_succ_locations(agent_to_move)

        if agent_to_move == 1:
            curr_max_score = float('-inf')
            curr_location = self.player_location
            for location in succ_locations:
                self.board[curr_location] = -1
                self.player_location = location
                self.board[location] = 1
                new_location, new_score, was_interrupted = self.rb_minimax(depth - 1, 2)
                if was_interrupted:
                    # print("Let's return!")
                    self.board[location] = 0
                    self.board[curr_location] = 1
                    self.player_location = curr_location
                    return (-1, -1), -1, True
                if new_score >= curr_max_score:
                    curr_max_score = new_score
                    best_move_location = location
                self.board[location] = 0
                self.board[curr_location] = 1
                self.player_location = curr_location
                if curr_max_score == float('inf'):
                    break
            return best_move_location, curr_max_score, False
        else:
            curr_min_score = float('inf')
            curr_location = self.rival_location
            for location in succ_locations:
                self.board[curr_location] = -1
                self.rival_location = location
                self.board[location] = 2
                new_location, new_score, was_interrupted = self.rb_minimax(depth - 1, 1)
                if was_interrupted:
                    self.board[location] = 0
                    self.board[curr_location] = 2
                    self.rival_location = curr_location
                    return (-1, -1), -1, True

                if new_score <= curr_min_score:
                    # print("I'm here")
                    curr_min_score = new_score
                    best_move_location = location

                self.board[location] = 0
                self.board[curr_location] = 2
                self.rival_location = curr_location
                if curr_min_score == float('-inf'):
                    break
            return best_move_location, curr_min_score, False

    def update_location(self, location, agent):
        if agent == 1:
            self.board[self.player_location] = -1
            self.board[location] = 1
            self.player_location = location
        else:
            self.board[self.rival_location] = -1
            self.board[location] = 2
            self.rival_location = location

    def board_area(self):
        return len(self.board) * len(self.board[0])

    def make_move(self, time_limit):
        self.start_time = time.time()
        self.time_for_move = time_limit
        depth = 1
        self.curr_iter_depth = 1
        best_move_location, best_score, was_interrupted = self.rb_minimax(depth, 1)
        # if best_score == float('inf') or best_score == float('-inf'):
        #     best_move = self.get_move(best_move_location)
        #     #print("Best move is ", best_move)
        #     self.update_location(best_move_location, 1)
        #     return best_move
        depth += 1
        self.curr_iter_depth += 1

        while self.time_left_for_move() > self.time_buffer and depth < self.board_area():
            new_move_location, new_score, was_interrupted = self.rb_minimax(depth, 1)
            if was_interrupted:
                # print("We've been interupted, boyz!")
                # print("Let's see our best move till now: ", best_move_location)
                break
            best_move_location, best_score = new_move_location, new_score
            # if best_score == float('inf') or best_score == float('-inf'):
            #     break
            depth += 1
            self.curr_iter_depth += 1
            # print("Best move location is ", best_move_location)

        best_move = self.get_move(best_move_location)
        # print("Best move is ", best_move)
        self.update_location(best_move_location, 1)
        # print("How much time it took: ", time.time() - self.start_time)
        # return best_move
        return depth

    def set_rival_move(self, location):
        self.update_location(location, 2)
